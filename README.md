# Zen Mode

Productivity Tool Demo

## Get started
1. Open Zen Mode
2. Drag N Drop the tutorial.map into the program
3. Click the loaded tutorial to learn more

## Functions
- Lists
- Images
- Drawings
- Connections

![](demo_images/zenmode_1.png)
## Privacy Mode
![](demo_images/zenmode_2.png)